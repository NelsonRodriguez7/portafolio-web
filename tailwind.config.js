//** @type {import('tailwindcss').Config} */
const { nextui } = require("@nextui-org/react");

export default {
  content: ["./index.html","./src/**/*.{js,jsx}",
          "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}"],
  theme: {
    colors:{
      //** Tus colores van aqui */
      'white':'#fafafa',
      'black':'#0a0a0a',
      'gray':'#393939'
    },
    fontFamily: {
      //** Tus Fuentes van aca */
      poppins: ['Poppins'],
    },
    animation: {
      'fade-in': 'fadeIn ease-out 1s',
      // animaciones 
    },
    extend: {},
  },
  
  plugins: [
    nextui({ prefix: "nextui", // prefix for themes variables
    addCommonColors: false, // override common colors (e.g. "blue", "green", "pink").
    defaultTheme: "light", // default theme from the themes object
    defaultExtendTheme: "light", // default theme to extend on custom themes
    layout: {}, // common layout tokens (applied to all themes)
    themes: {
      light: {
        layout: {}, // light theme layout tokens
        colors: {}, // light theme colors
      },
      dark: {
        layout: {}, // dark theme layout tokens
        colors: {}, // dark theme colors
      },
      // ... custom themes
    },}), require('tailwindcss-animated'), 
  ], 
}

