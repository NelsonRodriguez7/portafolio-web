export const Ventascard = [
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Vendemos paginas web para empresas",
      photo: "/building-skyscraper.svg",
      title: "Empresas",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Vendemos paginas web para emprendedores",
      photo: "/home-stats.svg",
      title: "Emprender",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Vendemos paginas web para escuelas",
      photo: "/books.svg",
      title: "Educación",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Vendemos paginas web para tiendas digitales",
      photo: "/shopping-bag.svg",
      title: "Tiendas",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Vendemos paginas web para investigaciones",
      photo: "/microscope.svg",
      title: "Investigación",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Vendemos plataformas web",
      photo: "/sitemap.svg",
      title: "Administración",
    },
];