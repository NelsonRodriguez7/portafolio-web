export const post = [
    {
    id: 1,
    date: "2023-10-08",
    datetime: "2023-10-08T00:00:00Z",

    

    category: {
      href: "#",
      title: "",
    },
    title: "Increíble servicio tecníco ",
    description: "Esta plataforma ofrece un conjunto de herramientas avanzadas que son esenciales para promover y hacer crecer sus negocios.",
    href: "#",
    author: {
      name: "Maria Perez",
      role: "Escritor",
      imageUrl: "/nataliaPhoto.jpg",
      authorHref: "#",
    },
    image: "/hero.webp",
    },
  
  {
    id: 2,
    date: "2023-10-09",
    datetime: "2023-10-09T00:00:00Z",
    category: {
      href: "#",
      title: "",
    },
    title: "Un sitio web excepcional ",
    description: "Esta plataforma ofrece un conjunto de herramientas avanzadas que son esenciales para promover y hacer crecer sus negocios. ",
    href: "#",
    author: {
      name: "Pedro Pacal ",
      role:"Marketing",
      imageUrl: "/Anthony.webp",
      authorHref: "#",},
      image: "/hero.webp",
      },
    
  

  // Puedes agregar más entradas aquí
  {
    id: 3,
    date: "2023-10-09",
    datetime: "2023-10-09T00:00:00Z",
    category: {
      href: "#",
      title: "",
    },
    title: "Notas Sobre su contenido",
    description: "Esta plataforma ofrece un conjunto de herramientas avanzadas que son esenciales para promover y hacer crecer sus negocios. ",
    href: "#",
    author: {
      name: "Juan Robert",
      role: "Ux Mannager ",
      imageUrl: "/Josting.jpeg",
      authorHref: "#",
    },
    image: "/hero.webp",
    },
  

];