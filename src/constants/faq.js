export const faq = [
    {
        title: "¿Que es un diseño y su importancia?",
        description: "Los recursos monetarios no son ilimitados, por ello una de las formas de medir el impacto o agrado de los clientes en tu futura página web y sus funciones, es con diseños que te permiten modificar por modicos precios frente al costo real de una página web completa; El diseño también es escencial para los desarrolladres que daran vida a tu negocio digital.",
    },
    {
        title: "¿Qué es el desarrollo de diseño?",
        description: "Si ya tienes tu diseño es hora de darle funcionalidad, desarrollar implica crear la página con un lenguaje de programación y etiquetas, para que tu web pueda ser desplegada en el internet.",
    },
    {
        title: "¿Puedo armar mi página web poco a poco con estos planes?",
        description: "Claro que si, hacer el diseño y desarrollo implica estar listo para desplegar en la web, solo hace falta comprar tu dominio web y seras visible para todo el mundo.",
    },
    {
        title: "¿Qué es la gestión de dominio?",
        description: "Se trata de la compra, configuración y administración para que tu página web que desarrollaste sea despleaga bajo el nombre de dominio que compraste, los dominios son las extensiones del sitio web ejemplo: scriptraptor.tech.",
    },
    {
        title: "¿Cómo son los mantenimientos?",
        description: "Al ser desarrollada tu página desde cero tendras muy pocos problemas, pero si nos dedicamos a darle mantemientos al SEO, el mencionado SEO es quien te permiete ser resutlado visible en las primeras busquedas de una persona en el navegador, ejemplo: si busco Panamá, tu sitio al contener palabras claves relacionadas tiene oportunidad de aparecer en esa busqueda.",
    },
    {
        title: "¿El login es seguro?",
        description: "Si, cumplimos estrictas normas de estandarización y seguridad, a la hora de hacer las conexiones a la base de datos, la base de datos nunca estara en contácto con la web, para ello se usa un intermediario creado por nosotros quien cumple funciones de seguridad para protegerla.",
    },
    {
        title: "¿Qué es el agendador de citas?",
        description: "Permite integrar a tu página web la agenda de citas automaticas sin que tengas que estar revisando si ese dia y la hora esta disponible para darle un servicio a tu cliente.",
    },
    {
        title: "¿Por qué tener un panel de control en tu web es importante?",
        description: "Permite que tengas el control sobre nueva información e imagenes que desees subir a tu web sin necesidad de pagar por que alguien más lo haga, funciona muy bien para negocios que quieren publicar promociónes.",
    },
    {
        title: "Tengo una página web y me esta dando problemas",
        description: "Te recomendamos migrarte con nosotros a las tegnologías que usamos pues es más facil que tu web cresca, mucho más barato y con estandares de seguridad, ademas podemos trabajar en nuevas funcionalidades mientras la paginá web esta en línea sin afectar su desenpeño.",
    },
    
];