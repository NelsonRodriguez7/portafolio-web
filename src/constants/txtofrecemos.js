export const CARD_TEXT = [
    {
      alt: "Cultivando tus ideas de negocios",
      photo: "/bulb.svg",
      title: "1",
      subtitle: "Incubadora de ideas",
      description: "Ponemos a tu dispoción una amplia variedad de paquetes promocionales, también tenemos pequeñas ofertas para que inicies a tu ritmo.",
    },
    {
      alt: "Desarrollo de tus ideas en una pagina web",
      photo: "/settings-code.svg",
      title: "2",
      subtitle: "Desarrollo de la incubadora",
      description: "Elaboremos estrategias para llegar a tus clientes potenciales y esas vistas a tus perfiles en linea se conviertan en compras de tus productos.",
    },
    {
      alt: "Pagina web de e-commerce o tienda digital",
      photo: "/checkup-list.svg",
      title: "3",
      subtitle: "Salida de la incubadora",
      description: "Ponemos a tu dispoción una amplia variedad de paquetes promocionales, también tenemos pequeñas ofertas para que inicies a tu ritmo.",
    },
    {
      alt: "Asistente 24 horas",
      photo: "/building-store.svg",
      title: "4",
      subtitle: "Obtén el asistente 24/7",
      description: "Elaboremos estrategias para llegar a tus clientes potenciales y esas vistas a tus perfiles en linea se conviertan en compras de tus productos.",
    },
  ];