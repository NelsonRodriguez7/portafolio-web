export const seguridad = [
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Documentación de código",
      photo: "/book-2.svg",
      title: "Documentación e informes",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Desarrollo de codigo fuente seguro",
      photo: "/code-circle-2.svg",
      title: "Desarrollo de código seguro",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Proteccion contra ciberataques",
      photo: "/lock.svg",
      title: "Protección contra ciberataques",
    },
    {
      link: "https://www.instagram.com/scriptraptor/",
      alt: "Monitoero o soperte tecnico",
      photo: "/device-imac-search.svg",
      title: "Soporte y mantenimiento",
    },
    
];