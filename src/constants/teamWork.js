export const teamWork = [
  {
    photo: "/nataliaPhoto.webp",
    alt:"Fotografia de Natalia Rojas, Diseñadora Ui / Ux en ScriptRaptor",
    name: "Natalia Rojas",
    role: "Diseñadora Ui / Ux",
    parrafo: "Estas son las tecnologias en la que apoyamos todo nuestro conocimientos y realizamos nuestros proyectos:",
    infotec: "Esta herramienta la utilizamos para el desarrollo backend XD",
    description1:
      "Encargada de la creación de los diseños de páginas webs y aplicaciones.",
    description2:
      "Cuenta con estudios universitarios en Desarrollo de Software y ha tomado cursos relacionados al área tecnológico.",
    socialNetwork: [
      { link: "https://www.facebook.com", icon: "/facebookIcon.svg" },
      { link: "https://www.instagram.com/nataliar_22/", icon: "/instagramIcon.svg" },
      { link: "https://www.linkedin.com", icon: "/linkedinIcon.svg" },
      { link: "https://github.com", icon: "/gitHubIcon.svg" },
    ],
  },
  {
    photo: "/foto_ig.webp",
    name: "Nelson Rodriguez",
    alt:"Fotografia de Nelson Rodriguez, Project manager en ScriptRaptor",
    role: "Project Manager",
    infotec: "Esta herramienta la utilizamos para el desarrollo Frontend Visca barca XD",
    description1:
      "Gestion y administración de los proyectos, a la vez también Full stack web.",
    description2:
      "Estudios universitarios en Desarrollo de Software y ha tomado cursos y certificaciones relacionados al área tecnológico.",
    socialNetwork: [
      { link: "https://www.facebook.com", icon: "/facebookIcon.svg" },
      { link: "https://www.instagram.com/thisnelson.y/", icon: "/instagramIcon.svg" },
      { link: "https://www.linkedin.com/in/nelson-rodriguez-902b8028b/", icon: "/linkedinIcon.svg" },
      { link: "https://github.com", icon: "/gitHubIcon.svg" },
    ],
  },
  {
    photo: "/tw.png",
    name: "Jose Mendoza",
    alt:"Fotografia de Jose Mendoza, Desarrollador web Full Stack en ScriptRaptor",
    role: "Full Stack Web",
    infotec: "Esta herramienta la utilizamos para el desarrollo  XD",
    description1:
      "Habilidades en el desarrollo Front-end y Back-end, manejo de base de datos",
    description2:
      "Cuenta con estudios universitarios en Desarrollo de Software y ha tomado cursos relacionados al área tecnológico.",
    socialNetwork: [
      { link: "https://www.facebook.com", icon: "/facebookIcon.svg" },
      { link: "https://www.instagram.com/jose_m2410/", icon: "/instagramIcon.svg" },
      { link: "https://www.linkedin.com", icon: "/linkedinIcon.svg" },
      { link: "https://github.com", icon: "/gitHubIcon.svg" },
    ],
  },
  {
    photo: "/Anthony.webp",
    name: "Anthony Gonzalez",
    alt:"Fotografia de Anthony Gonzalez, Desarrollador web Full Stack en ScriptRaptor",
    role: "Full Stack Web",
    infotec: "Esta herramienta la utilizamos para el desarrollo  XD",
    description1:
      "Habilidades Front-end y Back-end, especialista en animaciones y imágenes",
    description2:
      "Cuenta con estudios universitarios en Desarrollo de Software y ha tomado cursos relacionados al área tecnológico.",
    socialNetwork: [
      { link: "https://www.facebook.com/profile.php?id=100005500202546", icon: "/facebookIcon.svg" },
      { link: "https://www.instagram.com/anthonyjahirrr/", icon: "/instagramIcon.svg" },
      { link: "https://www.linkedin.com/in/anthony-gonz%C3%A1lez-aguilar-b4b60a253/", icon: "/linkedinIcon.svg" },
      { link: "https://github.com", icon: "/gitHubIcon.svg" },
    ],
  },
  {
    photo: "/Josting.webp",
    name: "Josting Ramos",
    alt:"Fotografia de Josting Ramos, Desarrollador web Full Stack en ScriptRaptor",
    role: "Full Stack Web",
    infotec: "Esta herramienta la utilizamos para el desarrollo  XD",
    description1:
      "Habilidades Front-end y Back-end, especialista en Marketing Digital y SEO",
    description2:
      "Cuenta con estudios universitarios en Desarrollo de Software y ha tomado cursos relacionados al área tecnológico.",
    socialNetwork: [
      { link: "https://www.facebook.com", icon: "/facebookIcon.svg" },
      { link: "https://www.instagram.com", icon: "/instagramIcon.svg" },
      { link: "https://www.linkedin.com", icon: "/linkedinIcon.svg" },
      { link: "https://github.com", icon: "/gitHubIcon.svg" },
    ],
  },
];
