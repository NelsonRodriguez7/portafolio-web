export const Beneficios = [
    {
        photo: "/device-mobile-message.svg",
        title: "Obtén 1 asesoría gratis cada mes",
        alt: "Asesoria",
        
    },
    {
        photo: "/coin-off.svg",
        title: "Precios accesibles al crecer tu web",
        alt: "Precios bajos",
        
    },
    {
        photo: "/clover.svg",
        title: "Oportunidad de ganar paquetes gratis",
        alt: "Oportunidades de ganar paquetes en ScriptRaptor",
       
    },
    {
        photo: "/phone-call.svg",
        title: "Obtén 1 mes de soporte gratis",
        alt: "Primer mes de regalo en ScriptRaptor",
        
    },
];