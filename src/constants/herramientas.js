export const Herramientas = [
    {
        photo: "/brand-javascript.svg",
        alt: "Logo de Javascript",
        title: "Javascript",
        title2: "L. Programación",
    },
    {
        photo: "/brand-react.svg",
        alt: "Logo de React",
        title: "React",
        title2: "Libreria",
    },
    {
        photo: "/brand-tailwind.svg",
        alt: "Logo de Tailwind CSS",
        title: "Tailwind Css",
        title2: "Estilos",
    },
    {
        photo: "/nodejs.svg",
        alt: "Logo de Node js",
        title: "Node js",
        title2: "Servidor web",
    },
    {
        photo: "/brand-mongodb.svg",
        alt: "Logo de Mongo DB",
        title: "MongoDB",
        title2: "Base de datos",
    },
    {
        photo: "/brand-php.svg",
        alt: "Logo de PHP",
        title: "PHP",
        title2: "L. Programación",
    },
];