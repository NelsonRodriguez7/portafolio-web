export const productos = [
    {
        title: "Diseño",
        price: "$30.00",
        type_pay: "Cada uno",
        description: "Diseño de página web o App movil por Ux / Ui profesionales",
        list:[
            {text:"2 días para entrega"},
            {text:"Entregado en figma"},
            {text:"modificables"}
        ],
    },
    {
        title: "Desarrollo de diseño web",
        price: "$65.00",
        type_pay: "Cada uno",
        description: "Tú página ya sera funcional para desplegar en la web",
        list:[
            {text:"3 días para entrega"},
            {text:"Fácil mantemiento"},
            {text:"Fácil despliegue"}
        ],
    },
    {
        title: "Despliegue de la web",
        price: "$20.00",
        type_pay: "Pago único",
        description: "Gestionamos tu nombre en la web y desplegamos tu sitio web",
        list:[
            {text:"1 día para entrega"},
            {text:"Configuración de dominio"},
            {text:"Certificados de seguridad"}
        ],
    },
    {
        title: "Mantenimiento web",
        price: "$25.00",
        type_pay: "Mensual",
        description: "Nos encargamos de lo técnico, tú de administrar el negocio",
        list:[
            {text:"Control de Seguridad"},
            {text:"Mitigación de errores"},
            {text:"Revisión SEO"}
        ],
    },
    {
        title: "Agrega Login a tu web",
        price: "$140.00",
        type_pay: "Pago único",
        description: "Permite a tus clientes iniciar sesión en tu sitio web",
        list:[
            {text:"Conexión a base de datos"},
            {text:"Seguridad de formularios"},
            {text:"Seguridad contra hackeos"}
        ],
    },
    {
        title: "Agrega agendador de citas",
        price: "$240.00",
        type_pay: "Pago único",
        description: "Permite a tus clientes agendar citas en tu sitio web",
        list:[
            {text:"Conexión a base de datos"},
            {text:"Panel de control incluido"},
            {text:"Genera reportes"}
        ],
    },
    {
        title: "Agrega panel de control",
        price: "$80.00",
        type_pay: "Pago único",
        description: "Permite que agregues nueva información a la web",
        list:[
            {text:"Conexión a base de datos"},
            {text:"Tú tienes el control"},
            {text:"Crea tus promociones"}
        ],
    },
    {
        title: "Agrega un cotizador",
        price: "$150.00",
        type_pay: "Pago único",
        description: "Permite que tus clientes generen cotizaciones",
        list:[
            {text:"Conexión a base de datos"},
            {text:"Genera un documento PDF"},
            {text:"Control de documentos"}
        ],
    },
    
];