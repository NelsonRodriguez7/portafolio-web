export const proyectosdatos = [
    {
        photo: "/proyecto1.webp",
        photologo: "/LogoFinal.png",
        photobackground:"/ico1card.webp",
        alt: "Instruct",
        title: "Instruct",
        context:"Personas de todo el mundo resuelven tus tareas.",
        url: "https://scriptraptor.tech/notfound",
        progress: "En Proceso",
    },
        {
        photo: "/proyecto2.webp",
        photologo: "/LogoFinal.png",
        photobackground:"/ico2card.webp",
        alt: "Portafolio",
        title: "Portafolio",
        context:"Página web de nuestro equipo de trabajo.",
        url: "https://scriptraptor.tech/",
        progress: "En Proceso",
    },
];