export const Weare = [
    {
      photo: "/LogoFinal.png",
      alt: "ScriptRaptor Desarrollo web Panama",
      role: "Descripción General",
      description1:
        "Encargada de la creación de los diseños páginas webs y aplicaciones.",
      description2:
        "Originarios de Panamá, combinamos la frescura de la juventud con la solidez de una formación de Lic. en Desarrollo de Software. Nacimos con la idea de ser los catalizadores de la revolución digital, brindando soluciones web y móviles a aquellos valientes emprendedores que se aventuran en nuevos retos. Nos enfocamos en construir la infraestructura tecnológica que respalda tus sueños de negocios digitales.",
      
    },
    {
      photo: "/mission.png",
      alt: "Mision de ScriptRaptor Empresa de desarrollo Web",
      role: "Nuestra Misión",
      description1:
        "Encargada de la creación de los diseños páginas webs y aplicaciones.",
      description2:
        "Guiados por la pasión de catalizar la transformación digital para pequeñas y medianas empresas, nos erigimos como faro de asesoría y desarrollo tecnológico. Nos emociona abrazar los desafíos propuestos por empresarios valientes. Nuestra dedicación se traduce en responsabilidad y transparencia, cultivando soluciones innovadoras. Nos especializamos en el nicho inexplorado de las PYME, garantizamos programas mantenibles, escalables y listos para la evolución tecnológica.",
      
    },
    {
      photo: "/vision2.png",
      alt: "Vision de ScriptRaptor Empresa de desarrollo web",
      role: "Nuestra Visión",
      description1:
        "Encargada de la creación de los diseños páginas webs y aplicaciones.",
      description2:
        "Aspiramos a liderar la vanguardia tecnológica en Panamá y expandirnos a toda Centroamérica. Nos posicionamos como aliados fundamentales para emprendedores digitales, proporcionando soluciones que van más allá del estándar. Buscamos ser el software principal de empresas, actualizándonos constantemente en tecnologías nuevas. Nuestra visión es ser reconocidos como líderes, catalizando el éxito de nuevos empresarios en el panorama digital con innovación, confianza y aprendizaje continuo.",
      
    },
    
  ];