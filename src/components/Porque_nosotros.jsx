import React, { useEffect, useState } from 'react';
import { Card, CardBody, Image } from "@nextui-org/react";
import { Beneficios } from "../constants/Beneficios";
import { Link } from "react-router-dom";
import { seguridad } from "../constants/seguridad";
import { motion } from 'framer-motion';
const Porque_nosotros = () => {
  
  return (
   <>
    <div className="h-full w-full font-poppins">
        <h2 className="mt-4 mx-6 text-2xl md:mt-4 md:mx-12 md:text-star md:text-4xl 2xl:text-5xl text-black font-semibold">
            Beneficios de trabajar con nosotros
        </h2>
        <div className=' mx-6 mt-6 rounded-md md:mx-12'>
            
            <div className="grid grid-cols-1 gap-4 md:mt-6 md:mx-12 md:grid-cols-2 md:gap-12 lg:grid-cols-4">
                {Beneficios.map((card, index) => (
                  <Card key={index} className=" shadow-none">
                    <div
                      className="flex items-center justify-center mt-4 md:mt-10">
                      <Image
                        alt={card.alt}
                        height={100}
                        src={card.photo}
                        width={100}
                      />
                    </div>
                    
                    <CardBody>
                      <p className="text-black text-center font-medium">{card.title}</p>
                      
                    </CardBody>
                  </Card>
                ))}
              </div>
          </div>
        <h2 className="mt-6 mx-6 text-center text-2xl md:mt-6 md:text-4xl 2xl:text-5xl text-black font-semibold">
                      Adicional te brindamos una seguridad robusta
                    </h2>
        <div className='h-full flex flex-col md:mb-0 md:mt-4 md:flex-row'> 
          <div className="flex flex-col h-full w-full md:w-1/2">
           
          <div className="mt-8 grid grid-cols-2 mx-6 md:mx-12 md:mt-20 md:mb-0 2xl:mt-32 gap-4">
                    {seguridad.map((segu, index) => (
                    <Link key={index} to={segu.link} target="_blank" rel="noopener noreferrer">
                    <motion.div  
                    whileHover={{ scale: 1.1 }}>
                    <Card  className="shadow-none">
                        <div className="ml-2 mr-2 mt-2 flex items-center justify-center">
                        <Image
                            alt={segu.alt}
                            height={70}
                            src={segu.photo}
                            width={70}
                        />
                        </div>
                        
                        <CardBody>
                        <p className="text-black text-center font-md text-sm md:text-base md:font-medium ">{segu.title}</p>
                        
                        </CardBody>
                    </Card>
                    </motion.div>
                    </Link>
                    ))}
                </div>
          </div>
            <div className='flex flex-col h-full w-full md:w-1/2 p-0 text-black'>
                
            <div className="mx-6 md:mx-12 object-cover animate-pulse">
                    
                    <img className="h-full w-full" alt="Normas del equipo de ScriptRaptor" src="/trabajo-en-grupo.webp"/>
                  </div>
            </div>
          </div>
      </div>
   </>
  )
}

export default Porque_nosotros