import { Link } from "react-router-dom"
import React from 'react'

const Footer = () => {
  return (
    <>
      <footer className="font-poppins bg-[#F2F2F2]">
          <div className="mx-auto w-full  p-4 py-6 lg:py-8">
              <div className="md:flex md:justify-between">
                <div className="mb-6 mx-6 md:mx-12 md:mb-0 sm:justify-center" >
                    <div className="flex items-center"> 
                        <span className="self-center text-xl font-semibold whitespace-nowrap ">ScriptRaptor</span>
                    </div>
                    
                    <div className='mt-6'>
                        <div className="flex items-center mt-4 space-x-5 sm:justify sm:mt-0 mb-6 md:mb-0">
                              <Link to="https://www.facebook.com/profile.php?id=61552194718297" className="text-gray-500 hover:text-gray-900 dark:hover:text-white">
                                  <img src="/facebook.svg" className="w-4 h-4" alt="Facebook Logo" />
                                  <span className="sr-only">Facebook page</span>
                              </Link>
                              <Link to="https://www.instagram.com/scriptraptor/" className="text-gray-500 hover:text-gray-900 dark:hover:text-white">
                                  <img src="/instagram.svg" className="w-5 h-4" alt="Instagram Logo" />
                                  <span className="sr-only">Instagram community</span>
                              </Link>
                              <Link to="#" className="text-gray-500 hover:text-gray-900 dark:hover:text-white">
                                  <img src="/youtube.svg" className="w-5 h-5" alt="Youtube Logo" />
                                  <span className="sr-only">Youtube Channel</span>
                              </Link>
                        </div>
                    </div>
                    
                </div>
                <div className="mx-6 md:mx-12 grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-3">
                    <div>
                        <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-black">Productos</h2>
                        <ul className="text-gray-500 dark:text-gray-400 font-medium">
                            <li className="mb-1">
                                <Link color="foreground" to="#" className="">Instruct</Link>
                            </li>
                           
                        </ul>
                    </div>
                    <div className="hidden md:block">
                        <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-black">Nosotros</h2>
                        <ul className="text-black dark:text-gray-400 font-medium">
                            <li className="mb-1">
                                <Link color="foreground" to="https://scriptraptor.tech/Contactanos" className=" ">Contáctanos</Link>
                            </li>
                            <li className="mb-1">
                                <Link color="foreground" to="https://scriptraptor.tech/Blog" className="">Blog</Link>
                            </li>
                            <li className="mb-1">
                                <Link color="foreground" to="https://scriptraptor.tech/Portafolio" className="">Portafolio</Link>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase text-black">Servicios</h2>
                        <ul className="text-gray-500 dark:text-gray-400 font-medium">
                            <li className="mb-1 ">
                                <Link color="foreground" to="https://scriptraptor.tech/" className="">Páginas web</Link>
                            </li>
                            <li className="mb-1">
                                <Link color="foreground" to="https://scriptraptor.tech/" className="">Asesoría digital</Link>
                            </li>
                            <li className="mb-1">
                                <Link color="foreground" to="https://scriptraptor.tech/" className="">SEO</Link>
                            </li>
                            <li className="mb-1">
                                <Link color="foreground" to="https://scriptraptor.tech/" className="">Marketing Digital</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <h2 className="mb-2 mx-6 mt-4 text-sm text-center font-medium text-black flex justify-center">Copyright © 2023 ScriptRaptor. Todos los derechos reservados.</h2>
          </div>
      </footer>

    </>
  )
}

export default Footer