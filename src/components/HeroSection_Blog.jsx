import React from "react";
import { Button } from "@nextui-org/react";
import { Link } from "react-router-dom";


const HeroSection_Blog = () => {
  const btnStyle =
    "rounded-3xl border-5 text-sm md:text-lg lg:text-xl font-extrabold w-24 md:w-28 lg:w-36 h-10 md:h-12 lg:h-12";

  return (
    <div className="HeroSection_Blog relative isolate overflow-hidden bg-gray-900 py-16 sm:py-24 lg:py-32 w-screen">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 lg:max-w-none lg:grid-cols-2">
          <div className="max-w-xl lg:max-w-lg">
            <h2 className="text-3xl font-bold tracking-tight text-black sm:text-4xl">
              Subscríbete a nuestro Blog.
            </h2>
            <p className="mt-4 text-lg leading-8 text-gray-300">
              Suscríbete a mi blog para recibir las últimas noticias y novedades
              sobre nosostros.
            </p>
            <div className="mt-6 flex max-w-md gap-x-4">
              <label htmlFor="email-address" className="sr-only">
                Email address
              </label>
              <input
                id="email-address"
                name="email"
                type="email"
                autoComplete="email"
                required
                className="min-w-0 flex-auto rounded-md border-0 bg-white/5 px-3.5 py-2 text-gray shadow-sm ring-1 ring-inset ring-white/10 focus:ring-2 focus:ring-inset focus:ring-indigo-500 sm:text-sm sm:leading-6"
                placeholder="Enter your email"
              />
              <button
                type="submit"
                className={`${btnStyle} border-black bg-transparent text-black hover:bg-black hover:text-white`}
              >
                Subscríbete
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeroSection_Blog;