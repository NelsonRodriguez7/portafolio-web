import React from "react";
import { Card, Button } from "@nextui-org/react";
import { Link } from "react-router-dom";
import { post } from "../constants/post";
import { motion, useAnimation } from "framer-motion";

const Entradas_blog = () => {
  const btnStyle =
    "rounded-3xl border-5 text-sm md:text-lg lg:text-xl font-extrabold w-24 md:w-28 lg:w-32 h-10 md:h-12 lg:h-16";

  return (
    <div className="container mx-auto max-w-7xl px-6 lg:px-8">
      <div className="mx-auto max-w-2xl lg:mx-0">
        <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
          Blog
        </h2>
        <p className="mt-2 text-lg leading-8 text-gray-600">
          Algunas Opiniones de Expertos sobre nuestro Trabajo.
        </p>
      </div>
      <div className="container mx-auto mt-10">
        <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 lg:grid-cols-3">
          {post.map((post) => (
            <Card key={post.id} shadow>
              <div className="p-4 relative group">
                <div className="group-hover:blur-md relative">
                  {/* Contenido del card */}
                  <h3 className="mt-3 text-lg font-semibold leading-6 text-gray-900 group-hover:text-gray-600">
                    <a href={post.href}>
                      <span className="absolute inset-0" />
                      {post.title}
                    </a>
                  </h3>
                  <img
                    src={post.image}
                    alt={post.title}
                    className="mt-3 rounded-lg w-full h-32 object-cover"
                  />
                  <p className="mt-5 line-clamp-3 text-sm leading-6 font-poppins">
                    {post.description}
                  </p>
                </div>
                <div className="relative mt-8 flex items-center gap-x-4">
           <img
             src={post.author.imageUrl}
             alt={post.author.name}
             className="h-10 w-10 rounded-full bg-gray-50"
             style={{ background: 'transparent' }}
           />
           <div className="text-sm leading-6">
             <p className="font-semibold text-gray-900">
               <a href={post.author.href}>
                 <span className="absolute inset-0" />
                 {post.author.name}
               </a>
             </p>
             <p className="text-gray-600">{post.author.role}</p>
           </div>
         </div>
         <div className="mt-5 text-base">
           <div className="rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100" style={{ fontFamily: 'Roboto, sans-serif' }}>
             {post.category.title}
           </div>
           
         </div>
                <div className="absolute inset-0 flex items-center justify-center">
                  <Button
                    className={`${btnStyle} border-black bg-black text-white hover:bg-[transparent] hover:text-black opacity-0 group-hover:opacity-100 transition-all duration-300 hidden group-hover:block`}
                  >
                    Ver más
                  </Button>
                  
                </div>
              </div>
            </Card>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Entradas_blog;