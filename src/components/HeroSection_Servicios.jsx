import React from 'react'
import { Button } from "@nextui-org/react";
import { Link } from "react-router-dom";
import { Card, CardBody,Image } from "@nextui-org/react";
import { Ventascard } from "../constants/Ventascard";
import { motion } from 'framer-motion';

const HeroSection_Servicios = () => {
    const btnStyle =
    "rounded-3xl border-5 text-sm md:text-lg lg:text-2xl font-extrabold w-44 md:w-48 lg:w-72 h-14 md:h-16 lg:h-20";
  return (
    <>
        <div className=' h-full w-full font-poppins'>
            <h1 className="text-star mx-6 md:mx-0 md:ml-12 text-4xl md:mt-8 md:text-5xl lg:text-6xl 2xl:text-7xl 2xl:mt-18  text-black font-semibold">
                ¿Necesitas una página web?
            </h1>
            
            <div className='h-full flex flex-col md:flex-row'>
            <div className='flex flex-col h-full w-full p-0 text-black'>
            <h2
                            style={{
                              WebkitTextStroke: "0.1px black",
                            }}
                            className="mx-6 text-base mt-4 md:mx-0 md:ml-12 md:text-base 2xl:text-xl text-black "
                            >
                             Aquí encontraras lo que necesitas para    
                          </h2>
            <div className="mt-8 grid grid-cols-2 mx-6 md:mx-14 md:mb-2 md:grid-cols-3 gap-4">
                    {Ventascard.map((ventas, index) => (
                    <Link key={index} to={ventas.link} target="_blank" rel="noopener noreferrer">
                    <motion.div 
                     
                    whileHover={{ scale: 1.1 }}>
                    <Card  className="bg-[white]">
                        <div className="ml-2 mr-2 mt-2 flex items-center justify-center">
                        <Image
                            alt={ventas.alt}
                            height={70}
                            src={ventas.photo}
                            width={70}
                        />
                        </div>
                        
                        <CardBody>
                        <p className="text-black text-center font-md text-sm md:text-base md:font-medium ">{ventas.title}</p>
                        
                        </CardBody>
                    </Card>
                    </motion.div>
                    </Link>
                    ))}
                </div>
                <div className=' text-center mt-6 md:mt-6'>
                    <span>¿Buscas algo más personalizado?</span>
                </div>
                <div className="mt-2 mb-2 md:mt-2 flex items-center justify-center gap-x-2 md:gap-x-6">
                
                <Link color="foreground" to="https://scriptraptor.tech/Contactanos">
                  <Button
                    className={`${btnStyle} border-black bg-black text-white hover:bg-[transparent] hover:text-black`}
                  >
                    Contáctanos
                  </Button>
                </Link>
                </div>
                
                
            </div>
                <div className="h-full w-full">
                  <div className="w-full h-[45vh] md:h-full animate-pulse">
                    <div className='mt-4 ml-6 mr-6 md:mr-12 md:ml-0 '>
                    <img className="h-full w-full rounded-lg" alt="Paginas web que vende ScriptRaptor" src="/paginas-web.webp"/>
                    </div>
                   
                  </div>
                </div>
            </div>
        </div>
    </>
  )
}

export default HeroSection_Servicios