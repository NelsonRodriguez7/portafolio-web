import { proyectosdatos } from '../constants/proyectosdatos';
import {Card, CardHeader, CardBody, CardFooter, Image, Button, Link} from "@nextui-org/react";
const Proyectoslista = () => {
    return (
        <>
         <div className="font-poppins grid grid-cols-1 mt-6 mx-6 sm:grid-cols-2 sm:gap-6 sm:mt-12 sm:mx-12 2xl:grid-cols-3 2xl:gap-8" >
                {proyectosdatos.map((value, index) => {
                    
                    return (
                        <Card key={index} isFooterBlurred className="mt-4">
                            <Image
                                removeWrapper
                                alt={value.alt}
                                className="z-0 w-full h-full object-cover"
                                src={value.photo}
                            />
                            <CardFooter className="absolute bg-black/40 bottom-0 z-10 border-t-1 border-default-600 dark:border-default-100">
                                <div className="flex flex-grow gap-2 items-center">
                                <Image
                                    alt={value.alt}
                                    className="rounded-full w-10 h-10"
                                    src={value.photologo}
                                />
                                <div className="flex flex-col">
                                        <p className="text-tiny text-white/60">{value.title}</p>
                                        <p className="text-tiny text-white/60">{value.progress}</p>
                                </div>
                                </div>                                  
                                <Button radius="full" size="sm" href={value.url} as={Link}>Visítala 
                                    <Image
                                        alt={value.alt}
                                        className="rounded-full w-5 h-7"
                                        src="/world.svg"
                                    />
                                </Button>
                            </CardFooter>
                            <div className="mt-4"></div> 
                            <div className="mt-4"></div> 
                            <div className="mt-4"></div> 
                        </Card> 
                    );        
                })}      
          </div>    
        </>
    )
};

export default Proyectoslista;