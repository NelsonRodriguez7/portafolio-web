import React from 'react';
import { Card, CardHeader, CardBody, CardFooter, Divider, Image } from "@nextui-org/react";
import { CARD_TEXT } from "../constants/txtofrecemos";
import { motion } from 'framer-motion';
import { Link } from "react-router-dom";
const Ofrecemos = () => {
 
  return (
    <>
      <div className="h-full w-full font-poppins">
        <h2 className="mt-4 mx-6 text-2xl md:mt-4 md:mx-0 md:text-center md:text-4xl 2xl:text-5xl text-black font-semibold">
            Nuestro proceso de creación web
        </h2>
        <div className="mt-8 grid grid-cols-2 mx-6 md:mx-14 md:mb-8 md:grid-cols-2 lg:grid-cols-4 gap-4">
        {CARD_TEXT.map((card, index) => (
          <Link key={index} to="https://www.instagram.com/scriptraptor/" target="_blank" rel="noopener noreferrer">
          <motion.div  
         
          whileHover={{ scale: 1.1 }}>
          <Card  className="shadow-md">
            <div className="ml-2 mr-2 mt-2 flex items-center justify-center">
              <Image
                alt={card.alt}
                height={100}
                src={card.photo}
                width={100}
              />
            </div>
            
            <CardBody>
              <p className="text-black text-center text-lg md:text-xl font-semibold">{card.title}</p>
              <p className="text-black text-center mb-2 md:text-lg">{card.subtitle}</p>
            </CardBody>
          </Card>
          </motion.div>
          </Link>
        ))}
      </div>
        <div className='h-full flex flex-col mt-6 md:mt-12 md:mb-8 md:flex-row'> 
          <div className="flex flex-col h-full w-full md:w-1/2">
            <div className='mt-4 mx-6 md:mt-12 md:mx-12 2xl:object-cover 2xl:mt-0'>
               <img alt='Despega tu empresa con la transformacion digital' className='w-full h-full rounded-md ' src='/transformacion-digital.webp'/>
            </div>
             
            
            
          </div>
          <div className='flex flex-col h-full w-full md:w-1/2 p-0 text-black'>
              <div className="w-full mb-8 sm:mb-8 sm:flex sm:justify-center">
                <div className="md:text-star">
                        <h2 className="mt-8 mx-6 text-center text-2xl md:mt-4 md:text-4xl 2xl:text-5xl text-black font-semibold">
                           Cómo te damos ese impulso con nuestos procesos.
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.2px black",
                            }}
                            className="mx-6 text-justify text-lg mt-4 md:text-base lg:text-xl 2xl:text-2xl text-black "
                            >
                              Incubadora de ideas:
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                              Aquí nos dedicamos a plasmar tus ideas en una página web concreta.  
                                 
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                              Creamos todo el branding, 
                              desde la selección de colores y fuentes hasta la disposición de la información e imágenes. 
                                 
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                               
                              Además, generamos las primeras vistas para mostrarte cómo luciría tu producto final.    
                          </h2>
                          
                          <h2
                            style={{
                              WebkitTextStroke: "0.2px black",
                            }}
                            className="mx-6 text-justify text-lg mt-4 md:text-base lg:text-xl 2xl:text-2xl text-black "
                            >
                              Desarrollo de la incubadora:
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                              Iniciamos la transformación del diseño final en un prototipo funcional. 
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                             Gracias a la 
                              implementación de DevOps en nuestro proceso de desarrollo, te brindamos la oportunidad 
                              de seguir de cerca el progreso de tu página web en tiempo real.     
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                              Puedes acceder a un 
                              enlace en cualquier momento para observar cómo se está desarrollando.    
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.2px black",
                            }}
                            className="mx-6 text-justify text-lg mt-4 md:text-base lg:text-xl 2xl:text-2xl text-black "
                            >
                              Salida de la incubadora:
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                              Tu producto está listo para ser utilizado, permitiéndote cargar toda la información adicional 
                              que desees.  
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.0px black",
                            }}
                            className="mx-6 text-justify text-base mt-1 md:mx-0 md:ml-6 md:mr-14 md:text-base 2xl:text-lg text-black "
                            >
                              Es el momento de embarcarnos en estrategias de marketing digital o compartir tu sitio
                              web con tus clientes actuales.    
                          </h2>
                      
                  </div>
              </div>
            
            </div>
          </div>
      </div>
    </>
  );
}

export default Ofrecemos;
