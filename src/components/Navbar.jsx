
import {Navbar, NavbarBrand, NavbarContent, NavbarItem,NavbarMenuToggle,NavbarMenu,NavbarMenuItem} from "@nextui-org/react";
import React, { useState } from 'react';
import { Link } from "react-router-dom"
/**------------------------------------Anthony---------------------------------------*/



export const Navbar2 = () => {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  const closeMenu = () => {
    setIsMenuOpen = React.useState(false)
  };

  const menuItems = [
    { label: "Blog", url: "/Blog" },
    { label: "Portafolio", url: "/Portafolio" },
    { label: "Contáctanos", url: "/Contactanos" },
    { label: "Servicios", url: "/Servicios" },
  ];
  return(
    <>
      
      <Navbar onMenuOpenChange={setIsMenuOpen} isBlurred={false} shouldHideOnScroll maxWidth="full">
      <NavbarContent>
        <NavbarMenuToggle
          icon = "="
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
          className="sm:hidden text-2xl"
        />
        <NavbarBrand>
        <Link to='https://scriptraptor.tech/'><img alt="Logo de ScriptRaptor" className="w-16 h-16 hidden sm:block md:block lg:block xl:block" src="/favicon-96x96.png"/></Link>
        <Link color="foreground" to="/"><p className="font-semibold  font-poppins">ScriptRaptor</p></Link> 
        </NavbarBrand>
      </NavbarContent>

      
      <NavbarContent justify="end" className="font-poppins">
      <Link to='https://scriptraptor.tech/'><img alt="Logo de ScriptRaptor" className="w-16 h-16 sm:hidden md:hidden lg:hidden xl:hidden" src="/favicon-96x96.png"/></Link>
      
      <NavbarItem className="hidden sm:block md:block lg:block xl:block 2xl:text-lg ">
          <Link color="foreground" to="/Blog">
            Blog
          </Link>
        </NavbarItem>
        <NavbarItem className="hidden sm:block md:block lg:block xl:block 2xl:text-lg">
          <Link color="foreground" to="/Portafolio">
            Portafolio
          </Link>
        </NavbarItem>
        <NavbarItem className="hidden sm:block md:block lg:block xl:block 2xl:text-lg">
          <Link color="foreground" to="/Contactanos">
            Contáctanos
          </Link>
        </NavbarItem>
        <NavbarItem className="hidden sm:block md:block lg:block xl:block 2xl:text-lg">
          <Link color="foreground" to="/Servicios">
            Servicios
          </Link>
        </NavbarItem>
      
      </NavbarContent>
      <NavbarMenu className="font-poppins">
        {menuItems.map((item, index) => (
          <NavbarMenuItem key={`${item.label}-${index}`}>
            <Link
              color= "foreground"
              className="w-full"
              to={item.url}
              size="lg"
              onClick={closeMenu}
            >
              {item.label}
            </Link>
          </NavbarMenuItem>
        ))}
      </NavbarMenu>
    </Navbar>
    </>
  );
}

export default Navbar2
