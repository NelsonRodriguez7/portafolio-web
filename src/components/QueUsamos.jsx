import React from 'react'
import {Herramientas} from "../constants/herramientas"
import { motion } from 'framer-motion';

/**---------------------------------------Jose--------------------------------------------------*/
const QueUsamos = () => {

 
  return (
    <>
      <div className="h-full w-full font-poppins">
        <div>
          <h2 className="mt-8 mx-6 text-2xl text-star md:mt-6 md:mx-12 md:text-4xl 2xl:mt-10 2xl:text-5xl text-black font-semibold">
            ¿Qué útilizamos para crear tu proyecto?
          </h2>
        </div>
        
        <div className='h-full flex flex-col md:mb-0 md:mt-4'> 
          <div className="flex flex-col h-full w-full">
            <h2
              style={{
                      WebkitTextStroke: "0.2px black",
                }}
                  className="mx-6 text-star mt-2 md:mt-0 md:mx-12 text-base lg:text-xl 2xl:mt-0 2xl:text-2xl text-black "
                              >
                    Descubre las herramientas tecnológicas y personal humano que se encargan de desarrollar tu idea.
            </h2> 
            <div className="bg-[#F2F2F2] mx-6 rounded-md grid grid-cols-1 md:mx-12 md:grid-cols-2 lg:grid-cols-3 justify-center items-center mt-6  mb-2 md:mb-0">
                {Herramientas.map((value, index) => {
                  return (
                <div
                  className="w-11/12 mb-4 mt-4 md:p-8 text-[black] text-center mx-auto md:w-11/12 lg:w-11/12 md:mb-0 "
                  key={index}
                >
                
                  <motion.div  
                  whileHover={{ scale: 1.1 }}
                  className="bg-[#F2F2F2] h-28 md:h-32  md:p-12 rounded-lg flex items-center">
                    <img
                      className="h-24 w-24 ml-4 md:ml-0 object-contain"
                      src={value.photo}
                      alt={value.alt}
                    />
                    <div className="flex-grow ml-2 md:ml-2">
                      <p className="text-black text-center text-lg font-medium md:text-left" >
                        {value.title}
                      </p>
                      <p className="text-black text-center text-xs font-medium md:text-left" >
                        {value.title2}
                      </p>
                    </div>
                    </motion.div>
                  
                </div>
              );
                  })}
              
              </div>
          
          </div>
          
          </div>
      </div>
    </>
  );
};

export default QueUsamos