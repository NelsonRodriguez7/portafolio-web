import React from 'react'
import {productos} from '../constants/productos'
import {faq} from '../constants/faq'
import { Button } from "@nextui-org/react";
import { Link } from "react-router-dom";
import {Accordion, AccordionItem} from "@nextui-org/react";

const Precios_Servicios = () => {
  const btnStyle =
  "rounded-3xl border-5 text-sm md:text-lg lg:text-xl font-medium w-44 md:w-36 lg:w-38 h-14 md:h-12 lg:h-12";
  return (
    <>
      <h2 className="mt-2  mx-6 text-2xl md:mt-8 md:mx-12 text-star md:text-4xl 2xl:text-5xl text-black font-semibold font-poppins">
            Mira nuestro catalogo
      </h2>
      
        <div className='h-full mb-8 mt-0'> 
          <div className="h-full w-full ">
          <h2
                            style={{
                              WebkitTextStroke: "0.1px black",
                            }}
                            className="mx-6 text-lg mt-2 md:mx-0 md:ml-12 md:text-xl 2xl:text-2xl text-black font-poppins"
                            >
                             Arma tu web con estos pequeños paquetes, puedes hacerlo poco a poco    
                          </h2>
          <div className="mt-4 grid grid-cols-1 mx-6 md:mx-12 md:mb-4 md:grid-cols-4 lg:grid-cols-4 gap-4 font-poppins">
          
          {productos.map((card, index) => (
            
            <div key={index} className="shadow p-5 rounded-lg border-t-4 border-[#353839]">
              <p className="text-sm font-medium text-gray-500">
                {card.title}
              </p>

              <p className="mt-4 text-3xl text-gray-700 font-medium">
                {card.price} <span className="text-base font-normal">/ {card.type_pay}</span>
              </p>

              <p className="mt-4 font-medium text-gray-700">
                 {card.description}
              </p>

              <div className="mt-8">
                <ul className="grid grid-cols-1 gap-4">
                {card.list.map((list, index) => {
                        return (
                  <li key={index} className="inline-flex items-center text-gray-600">
                    <svg className="w-4 h-4 mr-2 fill-[#353839] text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                      <path d="M256 0C114.6 0 0 114.6 0 256s114.6 256 256 256s256-114.6 256-256S397.4 0 256 0zM371.8 211.8l-128 128C238.3 345.3 231.2 348 224 348s-14.34-2.719-19.81-8.188l-64-64c-10.91-10.94-10.91-28.69 0-39.63c10.94-10.94 28.69-10.94 39.63 0L224 280.4l108.2-108.2c10.94-10.94 28.69-10.94 39.63 0C382.7 183.1 382.7 200.9 371.8 211.8z"></path>
                    </svg>
                    {list.text}
                  </li>
                );
              })}
                </ul>
              </div>

              <div className="mt-2 mb-2 md:mt-2 flex items-center justify-center gap-x-2 md:gap-x-6">
                
                <Link color="foreground" to="https://www.instagram.com/scriptraptor/">
                  <Button
                    className={`${btnStyle} border-black bg-black text-white hover:bg-[transparent] hover:text-black`}
                  >
                    Comprar
                  </Button>
                </Link>
                </div>
            </div>
             ))}
            
          </div>
          </div>
          <div className='flex flex-col h-full w-full  p-0 text-black'>
          <h2
                            style={{
                              WebkitTextStroke: "0.1px black",
                            }}
                            className="mx-6 text-lg mt-6 md:mt-2 md:mx-0 md:ml-12 md:text-xl 2xl:text-2xl text-black "
                            >
                             Preguntas frecuentes (FAQ)    
          </h2>
          <div className='mx-6 mt-4 md:mx-12 font-poppins text-justify'>
          <Accordion variant="shadow">
          {faq.map((card, index) => (
            <AccordionItem key={index} aria-label="Accordion 1" title={card.title}>
              {card.description}
            </AccordionItem>
           ))}  
          </Accordion>
          </div>
          </div>
        </div>
        
   
    
    </>
  )
}

export default Precios_Servicios