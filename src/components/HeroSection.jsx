import React from "react";
import { Button } from "@nextui-org/react";
import { Link } from "react-router-dom";


const HeroSection = () => {
  const btnStyle =
    "rounded-3xl border-5 text-sm md:text-lg lg:text-2xl font-extrabold w-32 md:w-40 lg:w-64 h-14 md:h-16 lg:h-20";

  return (
    <>
      <div className="h-full w-full font-poppins">
        
          <div className='h-full flex flex-col md:mb-8 md:flex-row '> 
          <div className='flex flex-col h-full w-full md:w-1/2 p-0 text-black '>
              <div className="w-full sm:mb-8 sm:flex sm:justify-center">
                <div className="md:text-star">
                  <h1 className="mx-6 md:mx-0 md:ml-12 text-4xl md:mt-16 md:text-5xl lg:text-6xl 2xl:text-7xl 2xl:mt-32 text-black font-semibold font-poppins">
                  Dale a tu negocio el impulso que necesita en esta era digital
                  </h1>
                  <h2
                            style={{
                              WebkitTextStroke: "0.1px black",
                            }}
                            className="mx-6 text-base mt-4 md:mx-0 md:ml-12 md:text-base 2xl:text-xl text-black"
                            >
                              En el mundo actual, donde la información se traduce en ventas, tener una presencia 
                              digital sólida es esencial.    
                          </h2>
                          <h2
                            style={{
                              WebkitTextStroke: "0.1px black",
                            }}
                            className="mx-6 text-base mt-0 md:mx-0 md:ml-12 md:text-base 2xl:text-xl text-black "
                            >
                              Una página web no es solo un activo en línea, 
                              es tu asistente en un mundo digital en constante cambio.    
                          </h2>
                          
                          

                  <div className="mt-10 md:mt-12 2xl:mt-16 flex items-center justify-center gap-x-2 md:gap-x-6">
                    <Link color="foreground" to="/Servicios">
                      <Button
                        className={`${btnStyle} border-black bg-transparent text-black hover:bg-black hover:text-white`}
                      >
                        Productos
                      </Button>
                    </Link>
                    <Link color="foreground" to="/Contactanos">
                      <Button
                        className={`${btnStyle} border-black bg-black text-white hover:bg-[transparent] hover:text-black`}
                      >
                        Contáctanos
                      </Button>
                    </Link>
                    
                  </div>
                  
                  
                </div>
              </div>
              
          </div>
                <div className="flex flex-col h-full w-full md:w-1/2">
                  <div className="w-full h-[45vh] md:h-full object-cover animate-pulse">
                    
                    <img className="h-full w-full" alt="Creacion de soluciones web en ScriptRaptor" src="/asistente-digital.webp"/>
                  </div>   
                </div>
          </div>
      </div>
    </>
  );
};

export default HeroSection;
