import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { teamWork } from "../constants/teamWork";

/**--------------------------------Josting---------------------------------------*/
const Equipo = () => {
  

  return (
    <>
      <div className="h-full w-full  font-poppins mb-8">
      <h2 className="mt-6 mx-6 text-center text-2xl md:mt-10 md:text-4xl 2xl:text-5xl text-black font-semibold">
                           Equipo de trabajo
                          </h2>
        <div className='h-full flex flex-col md:mb-0 md:mt-4 md:flex-row'> 
            <div className='text-black'>
              <ul role="list" className="mt-2 md:mt-8 grid grid-cols-1 mx-6 md:mx-12 md:mb-2 md:grid-cols-4 2xl:grid-cols-5 gap-4">
                <li>
                  <div className="flex items-center gap-x-6">
                    <img  className="h-24 w-24 md:h-40 md:w-40 rounded-full" src="/user-circle.svg" alt="Foto de Natalia Rojas Diseñadora de ScriptRaptor"/>
                    <div>
                      <h3 className="text-base font-semibold leading-7 tracking-tight text-black">Natalia Rojas</h3>
                      <p className="text-sm font-medium leading-6 ">Diseñadora Ux/Ui</p>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="flex items-center gap-x-6">
                    <img  className="h-24 w-24 md:h-40 md:w-40 rounded-full" src="/Nelson-Rodriguez.webp" alt="Foto de Nelson Rodriguez Lider de proyecto de ScriptRaptor"/>
                    <div>
                      <h3 className="text-base font-semibold leading-7 tracking-tight text-black">Nelson Rodríguez</h3>
                      <p className="text-sm font-medium leading-6 ">Lider de proyectos</p>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="flex items-center gap-x-6">
                    <img  className="h-24 w-24 md:h-40 md:w-40 rounded-full" src="/user-circle.svg" alt="Foto de Jose Mendoza Desarrollador full stack"/>
                    <div>
                      <h3 className="text-base font-semibold leading-7 tracking-tight text-black">Jóse Mendoza</h3>
                      <p className="text-sm font-medium leading-6 ">Desarrollador Full Stack</p>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="flex items-center gap-x-6">
                    <img  className="h-24 w-24 md:h-40 md:w-40 rounded-full" src="/Anthony-Gonzalez.webp" alt="Foto de Anthony Gonzalez desarrollador full stack de ScriptRaptor"/>
                    <div>
                      <h3 className="text-base font-semibold leading-7 tracking-tight text-black">Anthony Gonzalez</h3>
                      <p className="text-sm font-medium leading-6 ">Desarrollador Full stack</p>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="flex items-center gap-x-6">
                    <img  className="h-24 w-24 md:h-40 md:w-40 rounded-full" src="/Josting-Ramos.webp" alt="Foto de Josting Ramos desarrollador full stack de ScriptRaptor"/>
                    <div>
                      <h3 className="text-base font-semibold leading-7 tracking-tight text-black">Josting Ramos</h3>
                      <p className="text-sm font-medium leading-6 ">Desarrollador full stack</p>
                    </div>
                  </div>
                </li>
               
              </ul>  
            
            </div>
        </div>
      </div>
    </>
  );
};

export default Equipo;
