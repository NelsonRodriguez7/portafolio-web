import React from 'react'
import { Link } from 'react-router-dom';
import { Button } from '@nextui-org/react';

export const Contact = () => {
  const btnStyle =
    "rounded-3xl border-5 text-sm md:text-lg lg:text-2xl font-extrabold w-44 md:w-48 lg:w-72 h-14 md:h-16 lg:h-20";

  return (
    <>
      <div className="mt-4 mx-6 md:mx-12 font_poppins">
        <div className="mx-6 md:mx-12 text-center">
          <h2 className="text-center text-4xl md:mt-8 md:text-5xl lg:text-6xl 2xl:text-7xl 2xl:mt-18 text-black">Contacta con ventas</h2>
          <p className="text-start md:text-center text-base mt-4 md:text-base 2xl:text-xl text-black">Rellenar formulario para ponernos en contacto</p>
        </div>
        <form action="#" method="POST" className="mx-auto mt-16 max-w-xl sm:mt-20">
          <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
            <div>
              <label htmlFor="first-name" className="block text-sm font-medium leading-6 text-gray-900">Primer Nombre:</label>
              <div className="mt-2.5">
                <input type="text" name="first-name" id="first-name" autoComplete="given-name" className="block w-full rounded-md border-2 border-black px-3.5 py-2 text-black-900 shadow-sm ring-1 ring-inset ring-black-300"></input>
              </div>
            </div>
            <div>
              <label htmlFor="last-name" className="block text-sm font-medium leading-6 text-gray-900">Segundo Nombre:</label>
              <div className="mt-2.5">
                <input type="text" name="last-name" id="last-name" autoComplete="family-name" className="block w-full rounded-md border-2 border-black px-3.5 py-2 text-black-900 shadow-sm ring-1 ring-inset ring-black-300"></input>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label htmlFor="company" className="block text-sm font-medium leading-6 text-gray-900">Compañia:</label>
              <div className="mt-2.5">
                <input type="text" name="company" id="company" autoComplete="organization" className="block w-full rounded-md border-2 border-black px-3.5 py-2 text-black-900 shadow-sm ring-1 ring-inset ring-black-300"></input>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">Email:</label>
              <div className="mt-2.5">
                <input type="email" name="email" id="email" autoComplete="email" className="block w-full rounded-md border-2 border-black px-3.5 py-2 text-black-900 shadow-sm ring-1 ring-inset ring-black-300"></input>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label htmlFor="phone-number" className="block text-sm font-medium leading-6 text-gray-900">Numero telefonico:</label>
              <div className="relative mt-2.5">
                <div className="absolute inset-y-0 left-0 flex items-center">
                  <label htmlFor="country" className="sr-only">Country</label>
                  <select id="country" name="country" className="block w-full rounded-md border-2 border-black px-3.5 py-2 text-black-900 shadow-sm ring-1 ring-inset ring-black-300">
                    <option>US</option>
                    <option>CO</option>
                    <option>EU</option>
                    <option>PA</option>
                    <option>AR</option>
                  </select>
                </div>
                <input type="tel" name="phone-number" id="phone-number" autoComplete="tel" className="block w-full rounded-md border-2 border-black px-3.5 py-2 text-black-900 shadow-sm ring-1 ring-inset ring-black-300"></input>
              </div>
            </div>
            <div className="sm:col-span-2">
              <label htmlFor="message" className="block text-sm font-medium leading-6 text-gray-900">Detalles:</label>
              <div className="mt-2.5">
                <textarea name="message" id="message" rows="4" className="block w-full rounded-md border-2 border-black px-3.5 py-2 text-black-900 shadow-sm ring-1 ring-inset ring-black-300"></textarea>
              </div>
            </div>
            <div className="flex gap-x-4 sm:col-span-2">
              <div className="flex h-6 items-center">

                <button type="button" className="bg-gray-200 flex w-8 flex-none cursor-pointer rounded-full p-px ring-1 ring-inset ring-gray-900/5 transition-colors duration-200 ease-in-out focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600" role="switch" aria-checked="false" aria-labelledby="switch-1-label">
                  <span className="sr-only">Acepto el acuerdo</span>

                  <span aria-hidden="true" className="translate-x-0 h-4 w-4 transform rounded-full shadow-sm ring-1 ring-gray-900/5 transition duration-200 ease-in-out"></span>
                </button>
              </div>
              <label className="text-sm leading-6 text-gray-600" id="switch-1-label">
                By selecting this, you agree to our
                <a href="#" className="font-medium text-indigo-600"> privacy &nbsp;policy</a>.
              </label>
            </div>
          </div>
          <div className="mt-10">
            <div className="mt-2 mb-10 md:mt-2 flex items-center justify-center gap-x-2 md:gap-x-6">

              <Link color="foreground" to="https://scriptraptor.tech/Contactanos">
                <Button
                  className={`${btnStyle} border-black bg-black text-white hover:bg-[transparent] hover:text-black`}
                >
                  Enviar Mensaje
                </Button>
              </Link>
            </div>
          </div>
        </form>
      </div>
    </>

  )
}

export default Contact;