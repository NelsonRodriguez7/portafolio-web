import { useState } from 'react'
import Home from './pages/Home'
import {BrowserRouter,Routes,Route} from 'react-router-dom' 
import Navbar2 from './components/Navbar'
import Portafolio from './pages/Portafolio'
import Blog from './pages/Blog'
import Contactanos from './pages/Contactanos'
import Servicios from './pages/Servicios'
import NotFound from './pages/NotFound'
function App() {
 
  return (
    <>
      <BrowserRouter>
      <Navbar2></Navbar2>
      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/Portafolio" element={<Portafolio></Portafolio>}></Route>
        <Route path="/Blog" element={<Blog></Blog>}></Route>
        <Route path="/Servicios" element={<Servicios></Servicios>}></Route>
        <Route path="/Contactanos" element={<Contactanos></Contactanos>}></Route>
        <Route path="*" element={<NotFound></NotFound>}></Route>
      </Routes>
      </BrowserRouter>
    </>
  )
}

export default App
