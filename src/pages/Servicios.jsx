import React, {useEffect} from 'react'

import HeroSection_Servicios from '../components/HeroSection_Servicios';
import Precios_Servicios from '../components/Precios_Servicios';
import Footer from '../components/Footer';

const Servicios = () => {
  useEffect(() => {
    document.title = 'ScriptRaptor - Servicios web';
    // Puedes agregar lógica de limpieza si es necesario
    return () => {
      // Código de limpieza (si es necesario)
    };
  }, []);


  return (
    <>
    <HeroSection_Servicios></HeroSection_Servicios>
    <Precios_Servicios></Precios_Servicios>
    <Footer></Footer>
    </>
  )
}

export default Servicios