import React, {useEffect} from 'react'
import Proyectoslista from '../components/Proyectoslista';


const Portafolio = () => {
  useEffect(() => {
    document.title = 'ScriptRaptor | Portafolio';
    // Puedes agregar lógica de limpieza si es necesario
    return () => {
      // Código de limpieza (si es necesario)
    };
  }, []);


  return (
    <>
      <div className="mx-6 md:mx-0 md:ml-12 text-4xl md:mt-16 md:text-5xl lg:text-6xl 2xl:text-7xl 2xl:mt-32 text-black font-semibold font-poppins">Proyectos</div>
      <div className="mt-4"></div>
        <Proyectoslista></Proyectoslista>
      <div className="mt-4"></div>  
    </>
  )
}

export default Portafolio;