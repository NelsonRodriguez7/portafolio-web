import React, {useEffect, useState} from 'react'
import { Button } from "@nextui-org/react";
import { Link } from "react-router-dom"

const NotFound = () => {
  useEffect(() => {
    document.title = 'ScriptRaptor - Error 404';
    // Puedes agregar lógica de limpieza si es necesario
    return () => {
      // Código de limpieza (si es necesario)
    };
  }, []);

  const btnStyle = "rounded-3xl border-5 text-sm md:text-lg lg:text-2xl font-extrabold w-32 md:w-40 lg:w-64 h-14 md:h-16 lg:h-20";
  
  return (
    <>
        <div className={`transition-opacity duration-500 overflow-x-hidden`}>
        <div
          className="h-screen w-screen opacity-50 blur-xs flex items-center justify-center overflow-hidden"
          style={{
            backgroundImage: `url('/error.png')`,
            backgroundSize: "contain",
            backgroundPosition: "center center",
            backgroundRepeat: "no-repeat",
          }}
        ></div>
        <div className="mx-auto w-full py-32 sm:py-48 lg:py-5192b1b6 flex flex-col justify-center items-center absolute top-0 bottom-0 left-0 right-0">
          <div className="w-full sm:mb-8 sm:flex sm:justify-center">
            <div className="text-center">
              <h1 className="text-4xl md:text-base lg:text-7xl text-[#000000] font-semibold">
              ¡Oops!
              </h1>
              <h2 className="text-4xl md:text-base lg:text-7xl text-[#000000] font-semibold">
              ¿Se comieron esta página?
              </h2>
              <div className="mt-10 lg:mt-20 flex items-center justify-center gap-x-2 md:gap-x-6">
                <Link color="foreground" to="/"><Button
                  className={`${btnStyle} border-[#000000] bg-[#000000] text-[#FFFFFF] hover:bg-[transparent] hover:text-black`}
                >
                  Ir a Casa
            </Button></Link>
            </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default NotFound