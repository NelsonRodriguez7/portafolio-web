import React, {useEffect} from 'react'

import HeroSection from '../components/HeroSection'
import Navbar2 from '../components/Navbar'
import Ofrecemos from '../components/Ofrecemos'

import Equipo from '../components/Equipo'
import Footer from '../components/Footer'
import QueUsamos from '../components/QueUsamos'
import Porque_nosotros from '../components/Porque_nosotros'

const Home = () => {
  useEffect(() => {
    document.title = 'ScriptRaptor - Desarrollo web';
    // Puedes agregar lógica de limpieza si es necesario
    return () => {
      // Código de limpieza (si es necesario)
    };
  }, []);


  return (
    <> 
    <HeroSection></HeroSection>
    <Ofrecemos></Ofrecemos>
    <Porque_nosotros></Porque_nosotros>
    <QueUsamos></QueUsamos>
    
    <Equipo></Equipo>
    <Footer></Footer>
    </>
  )
}

export default Home