import React ,{useEffect} from 'react'
import Contact from '../components/Contact';

const Contactanos = () => {
  useEffect(() => {
    document.title = 'ScriptRaptor | Contactanos';
    // Puedes agregar lógica de limpieza si es necesario
    return () => {
      // Código de limpieza (si es necesario)
    };
  }, []);


  return (
    <Contact></Contact>
  )
}

export default Contactanos
