import React, { useEffect }  from 'react'

import Entradas_Blog from '../components/Entradas_blog';
import HeroSection_Blog from '../components/HeroSection_Blog';

const Blog = () => {
  useEffect(() => {
    document.title = 'ScriptRaptor | Blog';
    // Puedes agregar lógica de limpieza si es necesario
    return () => {
      // Código de limpieza (si es necesario)
    };
  }, []);


  return (
    <>
    <Entradas_Blog></Entradas_Blog>
    <HeroSection_Blog></HeroSection_Blog>
    </>
  )
}

export default Blog
